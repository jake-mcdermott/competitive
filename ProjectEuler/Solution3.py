import math
VALUE = 600851475143


def is_prime(n):
    if n % 2 == 0 and n > 2: 
        return False
    return all(n % i for i in xrange(3, int(math.sqrt(n)) + 1, 2))


for i in xrange(int(math.sqrt(VALUE)), 1, -1):
	if is_prime(i) and VALUE % i == 0:
		print i
		break
