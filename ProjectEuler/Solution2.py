class Memoize:
    def __init__(self, function):
        self.function = function
        self.memo = {}

    def __call__(self, *args):
        if not args in self.memo:
            self.memo[args] = self.function(*args)
        return self.memo[args]


def fibonacci(n):
    a, b = 0, 1
    for i in range(0, n):
        a, b = b, a + b
    return a


fibonacci = Memoize(fibonacci)

even_vals, i = 0, 1
while(fibonacci(i) < 4000000):
	val = fibonacci(i)
	if val % 2 == 0: even_vals += val
	i += 1
print even_vals